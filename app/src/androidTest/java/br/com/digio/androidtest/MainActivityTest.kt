package br.com.digio.androidtest

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.RecyclerViewMatchers.atPosition
import br.com.digio.androidtest.data.remote.service.DigioService
import br.com.digio.androidtest.presentation.MainActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivityTest {

    private val server = MockWebServer()

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    private val serviceTestModule = module {
        single<DigioService>(override = true) {
            Retrofit.Builder()
                .baseUrl("http://localhost:8080")
                .client(get())
                .addConverterFactory(GsonConverterFactory.create(get()))
                .build()
                .create(DigioService::class.java)
        }
    }

    @Before
    fun setUp() {
        loadKoinModules(serviceTestModule)
    }

    @After
    fun tearDown() {
        unloadKoinModules(serviceTestModule)
    }

    @Test
    fun onDisplayTitleShouldHaveHello() {
        launchActivity<MainActivity>().apply {
            val title = context.getString(R.string.hello_maria)

            moveToState(Lifecycle.State.RESUMED)

            onView(withText(title)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun onDisplayListSpotlightShouldRechargeItem() {
        server.dispatcher = MockServerUtil.dispatcherSuccess
        server.start(MockServerUtil.PORT)

        launchActivity<MainActivity>().apply {
            moveToState(Lifecycle.State.RESUMED)
            Thread.sleep(3000)
            onView(withId(R.id.recyMainSpotlight))
                .check(
                    matches(
                        atPosition(
                            0, hasDescendant(
                                withContentDescription("Recarga")
                            )
                        )
                    )
                )
        }
        server.close()
    }

    @Test
    fun onDisplayProductListShouldXboxItem() {
        server.dispatcher = MockServerUtil.dispatcherSuccess
        server.start(MockServerUtil.PORT)

        launchActivity<MainActivity>().apply {
            moveToState(Lifecycle.State.RESUMED)
            Thread.sleep(3000)
            onView(withId(R.id.recyMainProducts))
                .check(
                    matches(
                        atPosition(
                            0, hasDescendant(
                                withContentDescription("XBOX")
                            )
                        )
                    )
                )
        }
        server.close()
    }
}