package br.com.digio.androidtest.data.remote

import br.com.digio.androidtest.DIGIO_PRODUCTS_MOCK
import br.com.digio.androidtest.DIGIO_PRODUCTS_RESPONSE_MOCK
import br.com.digio.androidtest.data.remote.mapper.DigioProductsResponseToDigioProductsMapper
import br.com.digio.androidtest.data.remote.service.DigioService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DigioRemoteDataSourceTest {

    private lateinit var remoteDataSource: DigioRemoteDataSource

    private val serviceMock: DigioService = mockk(relaxed = true)
    private val digioProductsMapperMock: DigioProductsResponseToDigioProductsMapper = mockk(relaxed = true)

    @Before
    fun setUp() {
        remoteDataSource = DigioRemoteDataSource(serviceMock, digioProductsMapperMock)
    }

    @Test
    fun `when service returns a DigioProductsResponse then returns the mapped DigioProducts`() = runBlockingTest {
        // given
        coEvery { serviceMock.getProducts() } returns DIGIO_PRODUCTS_RESPONSE_MOCK
        coEvery { digioProductsMapperMock.map(DIGIO_PRODUCTS_RESPONSE_MOCK) } returns DIGIO_PRODUCTS_MOCK

        // when
        val result = remoteDataSource.getDigioProducts()

        // then
        assertEquals(DIGIO_PRODUCTS_MOCK, result)
    }

    @Test
    fun `when service throws an exception then returns null`() = runBlockingTest {
        // given
        coEvery { serviceMock.getProducts() } throws Exception()

        // when
        val result = remoteDataSource.getDigioProducts()

        // then
        assertEquals(null, result)
    }

}
