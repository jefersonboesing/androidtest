package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.SPOTLIGHT_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class SpotlightToSpotlightEntityMapperTest {

    private lateinit var mapper: SpotlightToSpotlightEntityMapper

    @Before
    fun setUp() {
        mapper = SpotlightToSpotlightEntityMapper()
    }

    @Test
    fun `when map from Spotlight then returns SpotlightEntity`() {
        // when
        val result = mapper.map(SPOTLIGHT_MOCK)

        // then
        assertEquals(SPOTLIGHT_MOCK.description, result.description)
        assertEquals(SPOTLIGHT_MOCK.bannerURL, result.bannerURL)
        assertEquals(SPOTLIGHT_MOCK.description, result.description)
    }

}
