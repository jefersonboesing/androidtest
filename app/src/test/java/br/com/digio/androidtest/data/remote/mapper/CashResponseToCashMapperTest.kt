package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.CASH_RESPONSE_MOCK
import br.com.digio.androidtest.data.remote.response.CashResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class CashResponseToCashMapperTest {

    private lateinit var mapper: CashResponseToCashMapper

    @Before
    fun setUp() {
        mapper = CashResponseToCashMapper()
    }

    @Test
    fun `when map from CashResponse with non-null values then returns Cash with all values`() {
        // when
        val result = mapper.map(CASH_RESPONSE_MOCK)

        // then
        assertEquals(CASH_RESPONSE_MOCK.bannerURL, result.bannerURL)
        assertEquals(CASH_RESPONSE_MOCK.title, result.title)
    }

    @Test
    fun `when map from CashResponse with null values then return Cash with empty values`() {
        // given
        val cashResponseMock = CashResponse(bannerURL = null, title = null)

        // when
        val result = mapper.map(cashResponseMock)

        // then
        assertTrue(result.bannerURL.isEmpty())
        assertTrue(result.title.isEmpty())
    }

}
