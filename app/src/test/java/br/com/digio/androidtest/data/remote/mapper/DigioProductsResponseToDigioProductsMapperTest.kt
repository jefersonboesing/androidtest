package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.CASH_MOCK
import br.com.digio.androidtest.CASH_RESPONSE_MOCK
import br.com.digio.androidtest.PRODUCT_MOCK
import br.com.digio.androidtest.PRODUCT_RESPONSE_MOCK
import br.com.digio.androidtest.SPOTLIGHT_MOCK
import br.com.digio.androidtest.SPOTLIGHT_RESPONSE_MOCK
import br.com.digio.androidtest.data.remote.response.DigioProductsResponse
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DigioProductsResponseToDigioProductsMapperTest {

    private lateinit var mapper: DigioProductsResponseToDigioProductsMapper

    private val cashMapperMock: CashResponseToCashMapper = mockk(relaxed = true)
    private val productMapperMock: ProductResponseToProductMapper = mockk(relaxed = true)
    private val spotlightMapperMock: SpotlightResponseToSpotlightMapper = mockk(relaxed = true)

    @Before
    fun setUp() {
        mapper = DigioProductsResponseToDigioProductsMapper(cashMapperMock, productMapperMock, spotlightMapperMock)
    }

    @Test
    fun `when map from DigioProductsResponse then returns DigioProducts`() {
        // given
        val digioProductsResponseMock = DigioProductsResponse(
            cash = CASH_RESPONSE_MOCK,
            products = listOf(PRODUCT_RESPONSE_MOCK),
            spotlight = listOf(SPOTLIGHT_RESPONSE_MOCK)
        )

        every { cashMapperMock.map(CASH_RESPONSE_MOCK) } returns CASH_MOCK
        every { productMapperMock.map(PRODUCT_RESPONSE_MOCK) } returns PRODUCT_MOCK
        every { spotlightMapperMock.map(SPOTLIGHT_RESPONSE_MOCK) } returns SPOTLIGHT_MOCK

        // when
        val result = mapper.map(digioProductsResponseMock)

        // then
        assertEquals(CASH_MOCK, result.cash)
        assertEquals(listOf(PRODUCT_MOCK), result.products)
        assertEquals(listOf(SPOTLIGHT_MOCK), result.spotlight)
    }

}
