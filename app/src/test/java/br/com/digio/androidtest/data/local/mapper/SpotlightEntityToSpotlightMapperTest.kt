package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.SPOTLIGHT_ENTITY_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class SpotlightEntityToSpotlightMapperTest {

    private lateinit var mapper: SpotlightEntityToSpotlightMapper

    @Before
    fun setUp() {
        mapper = SpotlightEntityToSpotlightMapper()
    }

    @Test
    fun `when map from SpotlightEntity then returns Spotlight`() {
        // when
        val result = mapper.map(SPOTLIGHT_ENTITY_MOCK)

        // then
        assertEquals(SPOTLIGHT_ENTITY_MOCK.description, result.description)
        assertEquals(SPOTLIGHT_ENTITY_MOCK.bannerURL, result.bannerURL)
        assertEquals(SPOTLIGHT_ENTITY_MOCK.description, result.description)
    }

}
