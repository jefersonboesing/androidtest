package br.com.digio.androidtest.data.local

import br.com.digio.androidtest.CASH_ENTITY_MOCK
import br.com.digio.androidtest.CASH_MOCK
import br.com.digio.androidtest.DIGIO_PRODUCTS_MOCK
import br.com.digio.androidtest.PRODUCT_ENTITY_MOCK
import br.com.digio.androidtest.PRODUCT_MOCK
import br.com.digio.androidtest.SPOTLIGHT_ENTITY_MOCK
import br.com.digio.androidtest.SPOTLIGHT_MOCK
import br.com.digio.androidtest.data.local.db.dao.CashDao
import br.com.digio.androidtest.data.local.db.dao.ProductDao
import br.com.digio.androidtest.data.local.db.dao.SpotlightDao
import br.com.digio.androidtest.data.local.mapper.CashEntityToCashMapper
import br.com.digio.androidtest.data.local.mapper.CashToCashEntityMapper
import br.com.digio.androidtest.data.local.mapper.ProductEntityToProductMapper
import br.com.digio.androidtest.data.local.mapper.ProductToProductEntityMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightEntityToSpotlightMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightToSpotlightEntityMapper
import br.com.digio.androidtest.domain.model.DigioProducts
import io.mockk.coEvery
import io.mockk.coVerifyOrder
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class DigioLocalDataSourceTest {

    private lateinit var localDataSource: DigioLocalDataSource

    private val cashDaoMock: CashDao = mockk(relaxed = true)
    private val productDaoMock: ProductDao = mockk(relaxed = true)
    private val spotlightDaoMock: SpotlightDao = mockk(relaxed = true)
    private val cashToEntityMapperMock: CashToCashEntityMapper = mockk(relaxed = true)
    private val productToEntityMapperMock: ProductToProductEntityMapper = mockk(relaxed = true)
    private val spotlightToEntityMapperMock: SpotlightToSpotlightEntityMapper = mockk(relaxed = true)
    private val entityToSpotlightMapperMock: SpotlightEntityToSpotlightMapper = mockk(relaxed = true)
    private val entityToProductMapperMock: ProductEntityToProductMapper = mockk(relaxed = true)
    private val entityToCashMapperMock: CashEntityToCashMapper = mockk(relaxed = true)

    @Before
    fun setUp() {
        localDataSource = DigioLocalDataSource(
            cashDaoMock,
            productDaoMock,
            spotlightDaoMock,
            cashToEntityMapperMock,
            productToEntityMapperMock,
            spotlightToEntityMapperMock,
            entityToSpotlightMapperMock,
            entityToProductMapperMock,
            entityToCashMapperMock
        )
    }

    @Test
    fun `when there is cash, products or spotlights in database then returns the DigioProducts`() = runBlockingTest {
        // given
        val productEntitiesMock = listOf(PRODUCT_ENTITY_MOCK)
        val spotlightEntitiesMock = listOf(SPOTLIGHT_ENTITY_MOCK)
        coEvery { cashDaoMock.getFirst() } returns CASH_ENTITY_MOCK
        coEvery { productDaoMock.getAll() } returns productEntitiesMock
        coEvery { spotlightDaoMock.getAll() } returns spotlightEntitiesMock
        coEvery { entityToCashMapperMock.map(CASH_ENTITY_MOCK) } returns CASH_MOCK
        coEvery { entityToProductMapperMock.map(PRODUCT_ENTITY_MOCK) } returns PRODUCT_MOCK
        coEvery { entityToSpotlightMapperMock.map(SPOTLIGHT_ENTITY_MOCK) } returns SPOTLIGHT_MOCK

        // when
        val result = localDataSource.getDigioProducts()

        // then
        val expected = DigioProducts(
            cash = CASH_MOCK,
            products = listOf(PRODUCT_MOCK),
            spotlight = listOf(SPOTLIGHT_MOCK)
        )
        assertEquals(expected, result)
    }

    @Test
    fun `when there is no cash, products and spotlights in database then returns null`() = runBlockingTest {
        // given
        coEvery { cashDaoMock.getFirst() } returns null
        coEvery { productDaoMock.getAll() } returns listOf()
        coEvery { spotlightDaoMock.getAll() } returns listOf()

        // when
        val result = localDataSource.getDigioProducts()

        // then
        assertEquals(null, result)
    }

    @Test
    fun `when update digio products then clear the database and insert new values`() = runBlockingTest {
        // given
        coEvery { cashDaoMock.deleteAll() } just runs
        coEvery { productDaoMock.deleteAll() } just runs
        coEvery { spotlightDaoMock.deleteAll() } just runs

        coEvery { cashToEntityMapperMock.map(CASH_MOCK) } returns CASH_ENTITY_MOCK
        coEvery { productToEntityMapperMock.map(PRODUCT_MOCK) } returns PRODUCT_ENTITY_MOCK
        coEvery { spotlightToEntityMapperMock.map(SPOTLIGHT_MOCK) } returns SPOTLIGHT_ENTITY_MOCK

        coEvery { cashDaoMock.insert(any()) } just runs
        coEvery { productDaoMock.insertAll(any()) } just runs
        coEvery { spotlightDaoMock.insertAll(any()) } just runs

        // when
        localDataSource.updateDigioProducts(DIGIO_PRODUCTS_MOCK)

        // then
        coVerifyOrder {
            cashDaoMock.deleteAll()
            productDaoMock.deleteAll()
            spotlightDaoMock.deleteAll()
            cashDaoMock.insert(CASH_ENTITY_MOCK)
            productDaoMock.insertAll(listOf(PRODUCT_ENTITY_MOCK))
            spotlightDaoMock.insertAll(listOf(SPOTLIGHT_ENTITY_MOCK))
        }

    }

}