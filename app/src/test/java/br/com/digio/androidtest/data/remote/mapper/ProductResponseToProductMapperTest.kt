package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.PRODUCT_RESPONSE_MOCK
import br.com.digio.androidtest.data.remote.response.ProductResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ProductResponseToProductMapperTest {

    private lateinit var mapper: ProductResponseToProductMapper

    @Before
    fun setUp() {
        mapper = ProductResponseToProductMapper()
    }

    @Test
    fun `when map from ProductResponse with non-null values then returns Product with all values`() {
        // when
        val result = mapper.map(PRODUCT_RESPONSE_MOCK)

        // then
        assertEquals(PRODUCT_RESPONSE_MOCK.imageURL, result.imageURL)
        assertEquals(PRODUCT_RESPONSE_MOCK.name, result.name)
        assertEquals(PRODUCT_RESPONSE_MOCK.description, result.description)
    }

    @Test
    fun `when map from ProductResponse with null values then return Product with empty values`() {
        // given
        val productResponseMock = ProductResponse(imageURL = null, name = null, description = null)

        // when
        val result = mapper.map(productResponseMock)

        // then
        assertTrue(result.imageURL.isEmpty())
        assertTrue(result.name.isEmpty())
        assertTrue(result.description.isEmpty())
    }

}
