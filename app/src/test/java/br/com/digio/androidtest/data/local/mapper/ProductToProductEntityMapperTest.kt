package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.PRODUCT_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class ProductToProductEntityMapperTest {

    private lateinit var mapper: ProductToProductEntityMapper

    @Before
    fun setUp() {
        mapper = ProductToProductEntityMapper()
    }

    @Test
    fun `when map from Product then returns ProductEntity`() {
        // when
        val result = mapper.map(PRODUCT_MOCK)

        // then
        assertEquals(PRODUCT_MOCK.description, result.description)
        assertEquals(PRODUCT_MOCK.imageURL, result.imageURL)
        assertEquals(PRODUCT_MOCK.description, result.description)
    }

}
