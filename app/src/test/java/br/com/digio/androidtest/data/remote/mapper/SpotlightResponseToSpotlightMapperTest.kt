package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.SPOTLIGHT_RESPONSE_MOCK
import br.com.digio.androidtest.data.remote.response.SpotlightResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class SpotlightResponseToSpotlightMapperTest {

    private lateinit var mapper: SpotlightResponseToSpotlightMapper

    @Before
    fun setUp() {
        mapper = SpotlightResponseToSpotlightMapper()
    }

    @Test
    fun `when map from SpotlightResponse with non-null values then returns Spotlight with all values`() {
        // when
        val result = mapper.map(SPOTLIGHT_RESPONSE_MOCK)

        // then
        assertEquals(SPOTLIGHT_RESPONSE_MOCK.bannerURL, result.bannerURL)
        assertEquals(SPOTLIGHT_RESPONSE_MOCK.name, result.name)
        assertEquals(SPOTLIGHT_RESPONSE_MOCK.description, result.description)
    }

    @Test
    fun `when map from SpotlightResponse with null values then return Spotlight with empty values`() {
        // given
        val spotlightResponseMock = SpotlightResponse(bannerURL = null, name = null, description = null)

        // when
        val result = mapper.map(spotlightResponseMock)

        // then
        assertTrue(result.bannerURL.isEmpty())
        assertTrue(result.name.isEmpty())
        assertTrue(result.description.isEmpty())
    }

}
