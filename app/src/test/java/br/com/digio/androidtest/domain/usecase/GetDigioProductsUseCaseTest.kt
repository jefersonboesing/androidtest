package br.com.digio.androidtest.domain.usecase

import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.domain.model.DigioProducts
import br.com.digio.androidtest.domain.repository.DigioRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class GetDigioProductsUseCaseTest {
    private lateinit var useCase: GetDigioProductsUseCase

    private val repositoryMock: DigioRepository = mockk(relaxed = true)

    @Before
    fun setUp() {
        useCase = GetDigioProductsUseCase(repositoryMock)
    }

    @Test
    fun `when invoke then returns the flow from the repository`() = runBlockingTest {
        // given
        val flowMock: Flow<Resource<DigioProducts>> = mockk()
        coEvery { repositoryMock.getDigioProducts() } returns flowMock

        // when
        val result = useCase.invoke()

        // then
        assertEquals(flowMock, result)
    }
}
