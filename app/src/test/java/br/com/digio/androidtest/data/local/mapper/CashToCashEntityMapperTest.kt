package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.CASH_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class CashToCashEntityMapperTest {

    private lateinit var mapper: CashToCashEntityMapper

    @Before
    fun setUp() {
        mapper = CashToCashEntityMapper()
    }

    @Test
    fun `when map from Cash then returns CashEntity`() {
        // when
        val result = mapper.map(CASH_MOCK)

        // then
        assertEquals(CASH_MOCK.bannerURL, result.bannerURL)
        assertEquals(CASH_MOCK.title, result.title)
    }

}
