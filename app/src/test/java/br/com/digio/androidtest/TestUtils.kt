package br.com.digio.androidtest

import br.com.digio.androidtest.data.local.db.entity.CashEntity
import br.com.digio.androidtest.data.local.db.entity.ProductEntity
import br.com.digio.androidtest.data.local.db.entity.SpotlightEntity
import br.com.digio.androidtest.data.remote.response.CashResponse
import br.com.digio.androidtest.data.remote.response.DigioProductsResponse
import br.com.digio.androidtest.data.remote.response.ProductResponse
import br.com.digio.androidtest.data.remote.response.SpotlightResponse
import br.com.digio.androidtest.domain.model.Cash
import br.com.digio.androidtest.domain.model.DigioProducts
import br.com.digio.androidtest.domain.model.Product
import br.com.digio.androidtest.domain.model.Spotlight

val CASH_RESPONSE_MOCK = CashResponse(bannerURL = "bannerUrl", title = "title")
val PRODUCT_RESPONSE_MOCK = ProductResponse(imageURL = "imageURL", name = "name", description = "description")
val SPOTLIGHT_RESPONSE_MOCK = SpotlightResponse(bannerURL = "bannerURL", name = "name", description = "description")

val CASH_MOCK = Cash(bannerURL = "bannerUrl", title = "title")
val PRODUCT_MOCK = Product(imageURL = "imageURL", name = "name", description = "description")
val SPOTLIGHT_MOCK = Spotlight(bannerURL = "bannerURL", name = "name", description = "description")

val CASH_ENTITY_MOCK = CashEntity(bannerURL = "bannerUrl", title = "title")
val PRODUCT_ENTITY_MOCK = ProductEntity(imageURL = "imageURL", name = "name", description = "description")
val SPOTLIGHT_ENTITY_MOCK = SpotlightEntity(bannerURL = "bannerURL", name = "name", description = "description")

val DIGIO_PRODUCTS_MOCK = DigioProducts(cash = CASH_MOCK, products = listOf(PRODUCT_MOCK), spotlight = listOf(SPOTLIGHT_MOCK))
val DIGIO_PRODUCTS_RESPONSE_MOCK = DigioProductsResponse(
    cash = CASH_RESPONSE_MOCK,
    products = listOf(PRODUCT_RESPONSE_MOCK),
    spotlight = listOf(SPOTLIGHT_RESPONSE_MOCK)
)