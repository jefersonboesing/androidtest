package br.com.digio.androidtest.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.digio.androidtest.DIGIO_PRODUCTS_MOCK
import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.domain.usecase.GetDigioProductsUseCase
import br.com.digio.androidtest.presentation.MainViewModel.ScreenState
import io.mockk.coEvery
import io.mockk.coVerifyOrder
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MainViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel

    private val getDigioProductsUseCaseMock: GetDigioProductsUseCase = mockk(relaxed = true)
    private val observeScreenStateMock: Observer<ScreenState> = mockk(relaxed = true)

    private val mainDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainDispatcher)
        viewModel = MainViewModel(getDigioProductsUseCaseMock)
        prepareLiveDataObservers()
    }

    @Test
    fun `when setup and get digio products use case emits a successful result then update screen state with the content state`() = runBlockingTest {
        // given
        coEvery { getDigioProductsUseCaseMock.invoke() } returns flow {
            emit(Resource.Loading)
            emit(Resource.Success(DIGIO_PRODUCTS_MOCK))
        }

        // when
        viewModel.setUp()

        // then
        coVerifyOrder {
            getDigioProductsUseCaseMock.invoke()
            observeScreenStateMock.onChanged(ScreenState.Loading)
            observeScreenStateMock.onChanged(
                ScreenState.Content(
                    cash = DIGIO_PRODUCTS_MOCK.cash,
                    products = DIGIO_PRODUCTS_MOCK.products,
                    spotlights = DIGIO_PRODUCTS_MOCK.spotlight
                )
            )
        }
    }

    @Test
    fun `when setup and get digio products use case emits an error result then update screen state with the error state`() = runBlockingTest {
        // given
        coEvery { getDigioProductsUseCaseMock.invoke() } returns flow {
            emit(Resource.Loading)
            emit(Resource.Error())
        }

        // when
        viewModel.setUp()

        // then
        coVerifyOrder {
            getDigioProductsUseCaseMock.invoke()
            observeScreenStateMock.onChanged(ScreenState.Loading)
            observeScreenStateMock.onChanged(ScreenState.Error)
        }
    }

    private fun prepareLiveDataObservers() = viewModel.run {
        screenState.observeForever(observeScreenStateMock)
    }

}
