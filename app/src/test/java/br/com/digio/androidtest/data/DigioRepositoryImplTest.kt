package br.com.digio.androidtest.data

import br.com.digio.androidtest.DIGIO_PRODUCTS_MOCK
import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.data.local.DigioLocalDataSource
import br.com.digio.androidtest.data.remote.DigioRemoteDataSource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DigioRepositoryImplTest {
    private lateinit var repository: DigioRepositoryImpl

    private val remoteDataSourceMock: DigioRemoteDataSource = mockk(relaxed = true)
    private val localDataSourceMock: DigioLocalDataSource = mockk(relaxed = true)

    @Before
    fun setUp() {
        repository = DigioRepositoryImpl(remoteDataSourceMock, localDataSourceMock)
    }

    @Test
    fun `when remote data source returns a DigioProducts then persist it and emit the DigioProducts`() = runBlockingTest {
        // given
        coEvery { remoteDataSourceMock.getDigioProducts() } returns DIGIO_PRODUCTS_MOCK
        coEvery { localDataSourceMock.updateDigioProducts(DIGIO_PRODUCTS_MOCK) } just runs

        // when
        val result = repository.getDigioProducts().toList()

        // then
        coVerify { localDataSourceMock.updateDigioProducts(DIGIO_PRODUCTS_MOCK) }
        assertEquals(listOf(Resource.Loading, Resource.Success(DIGIO_PRODUCTS_MOCK)), result)
    }

    @Test
    fun `when remote data source returns null and local data source returns a DigioProducts then emit the DigioProducts`() = runBlockingTest {
        // given
        coEvery { remoteDataSourceMock.getDigioProducts() } returns null
        coEvery { localDataSourceMock.getDigioProducts() } returns DIGIO_PRODUCTS_MOCK

        // when
        val result = repository.getDigioProducts().toList()

        // then
        coVerify(exactly = 0) { localDataSourceMock.updateDigioProducts(DIGIO_PRODUCTS_MOCK) }
        assertEquals(listOf(Resource.Loading, Resource.Success(DIGIO_PRODUCTS_MOCK)), result)
    }

    @Test
    fun `when remote data source returns null and local data source returns null then emit an Error`() = runBlockingTest {
        // given
        coEvery { remoteDataSourceMock.getDigioProducts() } returns null
        coEvery { localDataSourceMock.getDigioProducts() } returns null

        // when
        val result = repository.getDigioProducts().toList()

        // then
        coVerify(exactly = 0) { localDataSourceMock.updateDigioProducts(DIGIO_PRODUCTS_MOCK) }
        assertEquals(listOf(Resource.Loading, Resource.Error()), result)
    }

}
