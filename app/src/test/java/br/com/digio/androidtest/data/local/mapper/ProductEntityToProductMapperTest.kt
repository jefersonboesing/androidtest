package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.PRODUCT_ENTITY_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class ProductEntityToProductMapperTest {

    private lateinit var mapper: ProductEntityToProductMapper

    @Before
    fun setUp() {
        mapper = ProductEntityToProductMapper()
    }

    @Test
    fun `when map from ProductEntity then returns Product`() {
        // when
        val result = mapper.map(PRODUCT_ENTITY_MOCK)

        // then
        assertEquals(PRODUCT_ENTITY_MOCK.description, result.description)
        assertEquals(PRODUCT_ENTITY_MOCK.imageURL, result.imageURL)
        assertEquals(PRODUCT_ENTITY_MOCK.description, result.description)
    }

}
