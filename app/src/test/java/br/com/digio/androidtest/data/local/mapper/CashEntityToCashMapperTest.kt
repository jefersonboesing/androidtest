package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.CASH_ENTITY_MOCK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class CashEntityToCashMapperTest {

    private lateinit var mapper: CashEntityToCashMapper

    @Before
    fun setUp() {
        mapper = CashEntityToCashMapper()
    }

    @Test
    fun `when map from CashEntity then returns Cash`() {
        // when
        val result = mapper.map(CASH_ENTITY_MOCK)

        // then
        assertEquals(CASH_ENTITY_MOCK.bannerURL, result.bannerURL)
        assertEquals(CASH_ENTITY_MOCK.title, result.title)
    }

}
