package br.com.digio.androidtest.data.remote

import br.com.digio.androidtest.data.remote.mapper.DigioProductsResponseToDigioProductsMapper
import br.com.digio.androidtest.data.remote.service.DigioService
import br.com.digio.androidtest.domain.model.DigioProducts
import java.lang.Exception

class DigioRemoteDataSource(
    private val service: DigioService,
    private val digioProductsMapper: DigioProductsResponseToDigioProductsMapper
) {

    suspend fun getDigioProducts(): DigioProducts? {
        return try {
            val response = service.getProducts()
            digioProductsMapper.map(response)
        } catch (e: Exception) {
            null
        }
    }
}
