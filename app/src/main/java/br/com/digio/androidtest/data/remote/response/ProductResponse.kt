package br.com.digio.androidtest.data.remote.response

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("imageURL")
    val imageURL: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?
)
