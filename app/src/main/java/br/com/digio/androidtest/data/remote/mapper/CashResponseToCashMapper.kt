package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.commons.EMPTY_STRING
import br.com.digio.androidtest.data.remote.response.CashResponse
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Cash

class CashResponseToCashMapper : Mapper<CashResponse, Cash> {
    override fun map(input: CashResponse) = Cash(
        bannerURL = input.bannerURL ?: EMPTY_STRING,
        title = input.title ?: EMPTY_STRING
    )
}
