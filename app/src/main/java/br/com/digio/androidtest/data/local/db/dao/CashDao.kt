package br.com.digio.androidtest.data.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.digio.androidtest.data.local.db.entity.CashEntity

@Dao
interface CashDao {

    @Insert
    suspend fun insert(product: CashEntity)

    @Query("SELECT * FROM cash LIMIT 1")
    suspend fun getFirst(): CashEntity?

    @Query("DELETE FROM cash")
    suspend fun deleteAll()

}
