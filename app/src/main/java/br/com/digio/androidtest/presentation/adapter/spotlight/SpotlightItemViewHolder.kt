package br.com.digio.androidtest.presentation.adapter.spotlight

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.databinding.ItemMainSpotlightBinding
import br.com.digio.androidtest.domain.model.Spotlight
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class SpotlightItemViewHolder constructor(
    private val binding: ItemMainSpotlightBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Spotlight) {
        binding.imgMainItem.contentDescription = product.name
        binding.progressImage.isVisible = true

        Picasso.get()
            .load(product.bannerURL)
            .error(R.drawable.ic_alert_circle)
            .into(binding.imgMainItem, object : Callback {
                override fun onSuccess() {
                    binding.progressImage.isVisible = false
                }

                override fun onError(e: Exception?) {
                    binding.progressImage.isVisible = false
                }
            })
    }
}
