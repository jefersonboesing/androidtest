package br.com.digio.androidtest.domain.repository

import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.domain.model.DigioProducts
import kotlinx.coroutines.flow.Flow

interface DigioRepository {
    suspend fun getDigioProducts(): Flow<Resource<DigioProducts>>
}
