package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.ProductEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Product

class ProductToProductEntityMapper: Mapper<Product, ProductEntity> {
    override fun map(input: Product) = ProductEntity(
        imageURL = input.imageURL,
        name = input.name,
        description = input.description
    )
}
