package br.com.digio.androidtest.commons

sealed class Resource<out T> {
    object Loading : Resource<Nothing>()
    data class Error(val throwable: Throwable? = null) : Resource<Nothing>()
    data class Success<T>(val data: T) : Resource<T>()
}
