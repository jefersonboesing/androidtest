package br.com.digio.androidtest.domain.usecase

import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.domain.model.DigioProducts
import br.com.digio.androidtest.domain.repository.DigioRepository
import kotlinx.coroutines.flow.Flow

class GetDigioProductsUseCase(
    private val repository: DigioRepository
) {

    suspend operator fun invoke(): Flow<Resource<DigioProducts>> = repository.getDigioProducts()

}
