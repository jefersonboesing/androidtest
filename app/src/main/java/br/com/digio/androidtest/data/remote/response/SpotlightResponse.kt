package br.com.digio.androidtest.data.remote.response

import com.google.gson.annotations.SerializedName

data class SpotlightResponse(
    @SerializedName("bannerURL")
    val bannerURL: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?
)
