package br.com.digio.androidtest.data

import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.data.local.DigioLocalDataSource
import br.com.digio.androidtest.data.remote.DigioRemoteDataSource
import br.com.digio.androidtest.domain.model.DigioProducts
import br.com.digio.androidtest.domain.repository.DigioRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DigioRepositoryImpl(
    private val remoteDataSource: DigioRemoteDataSource,
    private val localDataSource: DigioLocalDataSource
) : DigioRepository {

    override suspend fun getDigioProducts(): Flow<Resource<DigioProducts>> = flow {
        emit(Resource.Loading)
        val digioProducts = remoteDataSource.getDigioProducts()
            ?.also { localDataSource.updateDigioProducts(it) }
            ?: localDataSource.getDigioProducts()
        if (digioProducts != null) emit(Resource.Success(digioProducts)) else emit(Resource.Error())
    }

}
