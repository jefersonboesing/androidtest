package br.com.digio.androidtest.domain

interface Mapper<K, V> {
    fun map(input: K): V?
}
