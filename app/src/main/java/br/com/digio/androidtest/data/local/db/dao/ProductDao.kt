package br.com.digio.androidtest.data.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.digio.androidtest.data.local.db.entity.ProductEntity

@Dao
interface ProductDao {

    @Insert
    suspend fun insertAll(product: List<ProductEntity>)

    @Query("SELECT * FROM product")
    suspend fun getAll(): List<ProductEntity>

    @Query("DELETE FROM product")
    suspend fun deleteAll()

}
