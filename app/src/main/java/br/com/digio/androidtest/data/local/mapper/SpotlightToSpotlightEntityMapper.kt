package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.SpotlightEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Spotlight

class SpotlightToSpotlightEntityMapper : Mapper<Spotlight, SpotlightEntity> {
    override fun map(input: Spotlight) = SpotlightEntity(
        bannerURL = input.bannerURL,
        name = input.name,
        description = input.description
    )
}
