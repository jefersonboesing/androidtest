package br.com.digio.androidtest.data.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.digio.androidtest.data.local.db.entity.SpotlightEntity

@Dao
interface SpotlightDao {

    @Insert
    suspend fun insertAll(product: List<SpotlightEntity>)

    @Query("SELECT * FROM spotlight")
    suspend fun getAll(): List<SpotlightEntity>

    @Query("DELETE FROM spotlight")
    suspend fun deleteAll()

}
