package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.commons.EMPTY_STRING
import br.com.digio.androidtest.data.remote.response.ProductResponse
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Product

class ProductResponseToProductMapper: Mapper<ProductResponse, Product> {
    override fun map(input: ProductResponse) = Product(
        imageURL = input.imageURL ?: EMPTY_STRING,
        name = input.name ?: EMPTY_STRING,
        description = input.description ?: EMPTY_STRING
    )
}
