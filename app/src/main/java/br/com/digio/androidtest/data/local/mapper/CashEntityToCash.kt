package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.CashEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Cash

class CashEntityToCashMapper : Mapper<CashEntity, Cash> {
    override fun map(input: CashEntity) = Cash(
        bannerURL = input.bannerURL,
        title = input.title
    )
}
