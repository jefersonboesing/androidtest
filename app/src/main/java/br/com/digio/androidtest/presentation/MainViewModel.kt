package br.com.digio.androidtest.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.commons.Resource
import br.com.digio.androidtest.domain.model.Cash
import br.com.digio.androidtest.domain.model.Product
import br.com.digio.androidtest.domain.model.Spotlight
import br.com.digio.androidtest.domain.usecase.GetDigioProductsUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainViewModel(
    private val getDigioProductsUseCase: GetDigioProductsUseCase
) : ViewModel() {

    private val _screenState = MutableLiveData<ScreenState>()
    val screenState: LiveData<ScreenState> = _screenState

    fun setUp() {
        dispatchGetDigioProducts()
    }

    private fun dispatchGetDigioProducts() = viewModelScope.launch {
        getDigioProductsUseCase().collect { resource ->
            _screenState.value = when (resource) {
                Resource.Loading -> ScreenState.Loading
                is Resource.Error -> ScreenState.Error
                is Resource.Success -> ScreenState.Content(
                    cash = resource.data.cash,
                    products = resource.data.products,
                    spotlights = resource.data.spotlight
                )
            }
        }
    }

    sealed class ScreenState {
        object Loading : ScreenState()
        object Error : ScreenState()
        data class Content(
            val cash: Cash?,
            val products: List<Product>,
            val spotlights: List<Spotlight>
        ) : ScreenState()
    }
}
