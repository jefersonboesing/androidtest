package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.CashEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Cash

class CashToCashEntityMapper : Mapper<Cash, CashEntity> {
    override fun map(input: Cash) = CashEntity(
        bannerURL = input.bannerURL,
        title = input.title
    )
}
