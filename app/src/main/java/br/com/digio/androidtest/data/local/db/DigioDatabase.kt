package br.com.digio.androidtest.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.digio.androidtest.data.local.db.dao.CashDao
import br.com.digio.androidtest.data.local.db.dao.ProductDao
import br.com.digio.androidtest.data.local.db.dao.SpotlightDao
import br.com.digio.androidtest.data.local.db.entity.CashEntity
import br.com.digio.androidtest.data.local.db.entity.ProductEntity
import br.com.digio.androidtest.data.local.db.entity.SpotlightEntity

@Database(entities = [CashEntity::class, ProductEntity::class, SpotlightEntity::class], version = DigioDatabase.DB_VERSION)
abstract class DigioDatabase : RoomDatabase() {
    abstract fun productsDao(): ProductDao
    abstract fun cashDao(): CashDao
    abstract fun spotlightDao(): SpotlightDao

    companion object {
        const val DB_NAME = "digio_cache.db"
        const val DB_VERSION = 1

        fun create(context: Context): DigioDatabase {
            return Room.databaseBuilder(context.applicationContext, DigioDatabase::class.java, DB_NAME).build()
        }
    }
}
