package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.ProductEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Product

class ProductEntityToProductMapper: Mapper<ProductEntity, Product> {
    override fun map(input: ProductEntity) = Product(
        imageURL = input.imageURL,
        name = input.name,
        description = input.description
    )
}
