package br.com.digio.androidtest.data.local

import br.com.digio.androidtest.data.local.db.dao.CashDao
import br.com.digio.androidtest.data.local.db.dao.ProductDao
import br.com.digio.androidtest.data.local.db.dao.SpotlightDao
import br.com.digio.androidtest.data.local.mapper.CashEntityToCashMapper
import br.com.digio.androidtest.data.local.mapper.CashToCashEntityMapper
import br.com.digio.androidtest.data.local.mapper.ProductEntityToProductMapper
import br.com.digio.androidtest.data.local.mapper.ProductToProductEntityMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightEntityToSpotlightMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightToSpotlightEntityMapper
import br.com.digio.androidtest.domain.model.Cash
import br.com.digio.androidtest.domain.model.DigioProducts
import br.com.digio.androidtest.domain.model.Product
import br.com.digio.androidtest.domain.model.Spotlight

class DigioLocalDataSource(
    private val cashDao: CashDao,
    private val productDao: ProductDao,
    private val spotlightDao: SpotlightDao,
    private val cashToEntityMapper: CashToCashEntityMapper,
    private val productToEntityMapper: ProductToProductEntityMapper,
    private val spotlightToEntityMapper: SpotlightToSpotlightEntityMapper,
    private val entityToSpotlightMapper: SpotlightEntityToSpotlightMapper,
    private val entityToProductMapper: ProductEntityToProductMapper,
    private val entityToCashMapper: CashEntityToCashMapper
) {

    suspend fun getDigioProducts(): DigioProducts? {
        val cash = cashDao.getFirst()?.let { entityToCashMapper.map(it) }
        val products = productDao.getAll().map { entityToProductMapper.map(it) }
        val spotlights = spotlightDao.getAll().map { entityToSpotlightMapper.map(it) }
        if (cash == null && products.isNullOrEmpty() && spotlights.isNullOrEmpty()) return null
        return DigioProducts(cash = cash, products = products, spotlight = spotlights)
    }

    suspend fun updateDigioProducts(digioProducts: DigioProducts) {
        clearDatabase()
        insertCash(digioProducts.cash)
        insertProducts(digioProducts.products)
        insertSpotlights(digioProducts.spotlight)
    }

    private suspend fun insertCash(cash: Cash?) {
        val cashEntity = cash?.let { cashToEntityMapper.map(cash) } ?: return
        return cashDao.insert(cashEntity)
    }

    private suspend fun insertProducts(products: List<Product>) {
        val productEntities = products.map { productToEntityMapper.map(it) }
        productDao.insertAll(productEntities)
    }

    private suspend fun insertSpotlights(spotlights: List<Spotlight>) {
        val spotlightEntities = spotlights.map { spotlightToEntityMapper.map(it) }
        spotlightDao.insertAll(spotlightEntities)
    }

    private suspend fun clearDatabase() {
        cashDao.deleteAll()
        productDao.deleteAll()
        spotlightDao.deleteAll()
    }

}
