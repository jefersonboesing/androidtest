package br.com.digio.androidtest.data.remote.response

import com.google.gson.annotations.SerializedName

data class DigioProductsResponse (
    @SerializedName("cash")
    val cash: CashResponse?,
    @SerializedName("products")
    val products: List<ProductResponse>?,
    @SerializedName("spotlight")
    val spotlight: List<SpotlightResponse>?
)
