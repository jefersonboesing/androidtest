package br.com.digio.androidtest.domain

import br.com.digio.androidtest.domain.usecase.GetDigioProductsUseCase
import org.koin.dsl.module

val domainModule = module {
    factory { GetDigioProductsUseCase(get()) }
}
