package br.com.digio.androidtest.data.remote.response

import com.google.gson.annotations.SerializedName

data class CashResponse(
    @SerializedName("bannerURL")
    val bannerURL: String?,
    @SerializedName("title")
    val title: String?
)
