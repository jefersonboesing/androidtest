package br.com.digio.androidtest.data

import br.com.digio.androidtest.commons.BASE_URL
import br.com.digio.androidtest.data.local.DigioLocalDataSource
import br.com.digio.androidtest.data.local.db.DigioDatabase
import br.com.digio.androidtest.data.local.mapper.CashEntityToCashMapper
import br.com.digio.androidtest.data.local.mapper.CashToCashEntityMapper
import br.com.digio.androidtest.data.local.mapper.ProductEntityToProductMapper
import br.com.digio.androidtest.data.local.mapper.ProductToProductEntityMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightEntityToSpotlightMapper
import br.com.digio.androidtest.data.local.mapper.SpotlightToSpotlightEntityMapper
import br.com.digio.androidtest.data.remote.DigioRemoteDataSource
import br.com.digio.androidtest.data.remote.mapper.CashResponseToCashMapper
import br.com.digio.androidtest.data.remote.mapper.DigioProductsResponseToDigioProductsMapper
import br.com.digio.androidtest.data.remote.mapper.ProductResponseToProductMapper
import br.com.digio.androidtest.data.remote.mapper.SpotlightResponseToSpotlightMapper
import br.com.digio.androidtest.data.remote.service.DigioService
import br.com.digio.androidtest.domain.repository.DigioRepository
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {
    single { DigioDatabase.create(get()) }
    factory { get<DigioDatabase>().cashDao() }
    factory { get<DigioDatabase>().productsDao() }
    factory { get<DigioDatabase>().spotlightDao() }
    factory { CashEntityToCashMapper() }
    factory { CashToCashEntityMapper() }
    factory { ProductEntityToProductMapper() }
    factory { ProductToProductEntityMapper() }
    factory { SpotlightEntityToSpotlightMapper() }
    factory { SpotlightToSpotlightEntityMapper() }
    single { DigioLocalDataSource(get(), get(), get(), get(), get(), get(), get(), get(), get()) }

    single { GsonBuilder().create() }
    single { OkHttpClient.Builder().build() }
    single<DigioService> {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
            .create(DigioService::class.java)
    }

    factory { CashResponseToCashMapper() }
    factory { ProductResponseToProductMapper() }
    factory { SpotlightResponseToSpotlightMapper() }
    factory { DigioProductsResponseToDigioProductsMapper(get(), get(), get()) }
    single { DigioRemoteDataSource(get(), get()) }

    single<DigioRepository> { DigioRepositoryImpl(get(), get()) }
}
