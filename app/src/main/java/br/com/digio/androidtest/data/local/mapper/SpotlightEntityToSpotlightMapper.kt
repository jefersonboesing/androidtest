package br.com.digio.androidtest.data.local.mapper

import br.com.digio.androidtest.data.local.db.entity.SpotlightEntity
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Spotlight

class SpotlightEntityToSpotlightMapper : Mapper<SpotlightEntity, Spotlight> {
    override fun map(input: SpotlightEntity) = Spotlight(
        bannerURL = input.bannerURL,
        name = input.name,
        description = input.description
    )
}
