package br.com.digio.androidtest.data.remote.service

import br.com.digio.androidtest.data.remote.response.DigioProductsResponse
import retrofit2.http.GET

interface DigioService {
    @GET("sandbox/products")
    suspend fun getProducts(): DigioProductsResponse
}
