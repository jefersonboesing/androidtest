package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.commons.EMPTY_STRING
import br.com.digio.androidtest.data.remote.response.SpotlightResponse
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.Spotlight

class SpotlightResponseToSpotlightMapper : Mapper<SpotlightResponse, Spotlight> {
    override fun map(input: SpotlightResponse) = Spotlight(
        bannerURL = input.bannerURL ?: EMPTY_STRING,
        name = input.name ?: EMPTY_STRING,
        description = input.description ?: EMPTY_STRING
    )
}
