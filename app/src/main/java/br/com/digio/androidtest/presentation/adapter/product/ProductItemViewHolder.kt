package br.com.digio.androidtest.presentation.adapter.product

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.databinding.ItemMainProductsBinding
import br.com.digio.androidtest.domain.model.Product
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class ProductItemViewHolder constructor(
    private val binding: ItemMainProductsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Product) {
        binding.imgMainItem.contentDescription = product.name
        binding.progressImage.isVisible = true

        Picasso.get()
            .load(product.imageURL)
            .error(R.drawable.ic_alert_circle)
            .into(binding.imgMainItem, object : Callback {
                override fun onSuccess() {
                    binding.progressImage.isVisible = false
                }

                override fun onError(e: Exception?) {
                    binding.progressImage.isVisible = false
                }
            })
    }
}
