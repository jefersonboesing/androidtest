package br.com.digio.androidtest.presentation

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.androidtest.R
import br.com.digio.androidtest.databinding.ActivityMainBinding
import br.com.digio.androidtest.domain.model.Cash
import br.com.digio.androidtest.presentation.MainViewModel.ScreenState
import br.com.digio.androidtest.presentation.adapter.product.ProductAdapter
import br.com.digio.androidtest.presentation.adapter.spotlight.SpotlightAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModel()

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupProductsList()
        setupSpotlightsList()
        setupObservables()
        viewModel.setUp()
    }

    private fun setupProductsList() = binding.apply {
        recyMainProducts.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
        recyMainProducts.adapter = productAdapter
    }

    private fun setupSpotlightsList() = binding.apply {
        recyMainSpotlight.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
        recyMainSpotlight.adapter = spotlightAdapter
    }

    private fun setupObservables() = viewModel.run {
        screenState.observe(this@MainActivity) { setupState(it) }
    }

    private fun setupState(state: ScreenState) {
        when (state) {
            is ScreenState.Content -> setupContentState(state)
            ScreenState.Error -> setupErrorState()
            ScreenState.Loading -> setupLoadingState()
        }
    }

    private fun setupContentState(state: ScreenState.Content) = binding.apply {
        loadDigioContainer.root.isVisible = false
        body.isVisible = true
        productAdapter.products = state.products
        spotlightAdapter.spotlights = state.spotlights
        setupDigioCashText(state.cash)
    }

    private fun setupErrorState() = binding.apply {
        loadDigioContainer.root.isVisible = false
        body.isVisible = false
        val message = getString(R.string.error)
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun setupLoadingState() = binding.apply {
        body.isVisible = false
        loadDigioContainer.root.isVisible = true
    }

    private fun setupDigioCashText(cash: Cash?) {
        val cashTitle = cash?.title ?: return
        val cashTitleSegments = cashTitle.split(" ")
        val firstWordEndIndex = cashTitleSegments.first().length
        binding.txtMainDigioCash.text = SpannableString(cashTitle).apply {
            setSpan(
                ForegroundColorSpan(ContextCompat.getColor(this@MainActivity, R.color.blue_darker)),
                0,
                firstWordEndIndex,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            if (cashTitleSegments.size <= 1) return
            setSpan(
                ForegroundColorSpan(ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)),
                firstWordEndIndex.inc(),
                cashTitle.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}