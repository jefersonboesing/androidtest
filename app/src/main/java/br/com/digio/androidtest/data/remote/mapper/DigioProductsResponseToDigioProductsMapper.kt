package br.com.digio.androidtest.data.remote.mapper

import br.com.digio.androidtest.data.remote.response.DigioProductsResponse
import br.com.digio.androidtest.domain.Mapper
import br.com.digio.androidtest.domain.model.DigioProducts

class DigioProductsResponseToDigioProductsMapper(
    private val cashMapper: CashResponseToCashMapper,
    private val productMapper: ProductResponseToProductMapper,
    private val spotlightMapper: SpotlightResponseToSpotlightMapper,
) : Mapper<DigioProductsResponse, DigioProducts> {
    override fun map(input: DigioProductsResponse) = DigioProducts(
        cash = input.cash?.let { cashMapper.map(input.cash) },
        products = input.products?.map { productMapper.map(it) } ?: listOf(),
        spotlight = input.spotlight?.map { spotlightMapper.map(it) } ?: listOf(),
    )
}
